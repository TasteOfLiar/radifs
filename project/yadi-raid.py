import stat
import errno
import fuse
from time import time
import raid_utils

#    def _full_path(self, partial):
#        partial = partial.lstrip("/")
#        path = os.path.join(self.root, partial)
#        return path


class MyStat(fuse.Stat):
    def __init__(self):
        self.st_mode = stat.S_IFDIR | 0o755
        self.st_ino = 0
        self.st_dev = 0
        self.st_nlink = 2
        self.st_uid = 0
        self.st_gid = 0
        self.st_size = 0
        self.st_atime = 0
        self.st_mtime = 0
        self.st_ctime = 0


class RadiFS(fuse.Fuse):
    def __init__(self, *args, **kw):
        fuse.Fuse.__init__(self, *args, **kw)
        self.files = {}
        self.raid = raid_utils.Raid()
        self.raid.initialize()

    def getattr(self, path):
        st = MyStat()
        attrs = self.raid.getfilemeta(path)
        
        st.st_atime = int(time())
        st.st_mtime = attrs['modified']  # modify time
        st.st_ctime = attrs['created']  # change time
        st.st_size = attrs['size']

    def readdir(self, path, offset):
        dirents = ['.', '..']
        if path == '/':
            dirents.extend(['Trash'])
        listings = self.dirList(path)
        for listing in listings:
            for file in listing:
                dirents.extend([file['name']])
        for r in dirents:
            yield fuse.Direntry(r)


# TODO: implement create file function
# Algo:
#   1. upload blank file to one of all disks
#   2. profit

    def mknod(self, path, mode, dev):
        
        return 0
