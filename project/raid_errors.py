# -*- coding: utf-8 -*-
"""
Created on Wed Jan  9 22:58:53 2019

@author: TasteOfLiar
"""


class Error(Exception):
    def __init__(self, expression='', message=''):
        self.expression = expression
        self.message = message    


class PathNotFoundError(Error):
    pass


class NotEnoughFreeSpaceError(Error):
    pass
