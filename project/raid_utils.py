# -*- coding: utf-8 -*-
"""
Created on Sun Jan  6 17:32:10 2019

@author: TasteOfLiar
"""

import yadisk
import raid_errors
import pickle
import config
import sys
import os
import logging

logging.basicConfig(format=config.LOG_FORMAT, level=logging.DEBUG, filename=u'raid.log')
logger = logging.getLogger('raid-utils')


class Raid(object):

    def __init__(self):
        self.__tokens = []
        self.__yadi = []
        self._disk_info = []

    def initialize(self):
        logger.info('Initialization started...')
        if os.path.exists(config.CACHE_FILE):
            self.__uncachetokens()
        else:
            self.__gettoken()
        for i in range(len(config.DISKS)):
            logger.info('Logging in to Yandex.Disk')
            self.__yadi.append(yadisk.YaDisk(config.DISKS[i]['id'],
                                             config.DISKS[i]['password'],
                                             self.__tokens[i]))
        self._disk_info = self.getdiskinfo()
        for disk in self._disk_info:
            logger.info('Using disk {}:'.format(disk['disk_name']))
        logger.info('Initialization ended')
        logger.info('---------------------------')

    def remove(self, path):
        self._disk_info = self.getdiskinfo()
        for y in self.__yadi:
            if y.exists(path):
                logger.info('({}) Removing file/directory ({})'.format(next(self._disk_info)['disk_name'], path))
                y.remove(path)
                return
            else:
                pass
        logger.warning('File/directory ({}) not found'.format(next(self._disk_info)['disk_name'], path))
        raise raid_errors.PathNotFoundError

    def rename(self, old, new):
        self._disk_info = self.getdiskinfo()
        for y in self.__yadi:
            if y.exists(old):
                logger.info('({}) Renaming file/directory ({}) to {}'.format(next(self._disk_info)['disk_name'], old, new))
                y.move(old, new)
                return
            else:
                pass
        logger.warning('File/directory ({}) not found'.format(next(self._disk_info)['disk_name'], old))
        raise raid_errors.PathNotFoundError

    def getdiskinfo(self):
        for ya in self.__yadi:
            disk_info = ya.get_disk_info()
            logger.info('Getting disk ({}) info'.format(disk_info['user']['login']))
            yield {
                'disk_name': disk_info['user']['login'],
                'total_space': disk_info['total_space'],
                'used_space': disk_info['used_space'],
                'free_space': (disk_info['total_space'] - disk_info['used_space']),
                }

    def dirlist(self, path):
        self._disk_info = self.getdiskinfo()
        for y in self.__yadi:
            if y.exists(path):            
                logger.info('({}) Getting directory ({}) listing'.format(next(self._disk_info)['disk_name'], path))
                files = y.listdir(path)
                for file in files:            
                    yield {
                        'name': file['name'],         
                        'size': file['size'],
                        'md5': file['md5'],
                        'sha256': file['sha256'],
                        'modified': file['modified'],
                        'created': file['created'],
                        'path': file['path'],
                        'type': file['type'],
                        }
            else:
                pass
        logger.warning('Dir ({}) not found'.format(path))
        raise raid_errors.PathNotFoundError

    def getfilemeta(self, path):
        self._disk_info = self.getdiskinfo()
        for y in self.__yadi:
            if y.exists(path):
                logger.info('({}) Getting file ({}) meta'.format(next(self._disk_info)['disk_name'], path))
                file = y.get_meta(path)
                return {
                    'name': file['name'],         
                    'size': file['size'],
                    'md5': file['md5'],
                    'sha256': file['sha256'],
                    'modified': file['modified'],
                    'created': file['created'],
                    'type': file['type'],
                }
            else:
                pass
        logger.warning('({}) File ({}) not found'.format(next(self._disk_info)['disk_name'], path))
        raise raid_errors.PathNotFoundError

    def upload(self, path, dst_path):
        self._disk_info = self.getdiskinfo()
        pe = path.split('/')
        for ya in self.__yadi:
            for info in self._disk_info:
                logger.info('Checking free space on disk {}:...'.format(info['disk_name']))
                if info['free_space'] >= os.path.getsize(path):
                    logger.info('{}: has enough free space ({} B) for file {}({} B)'.format(info['disk_name'],
                                info['free_space'],
                                path,
                                os.path.getsize(path)))
                    logger.info('Uploading {} to {}:{}'.format(path, info['disk_name'], dst_path))
                    ya.upload(path, dst_path + pe[-1])
                    return
                else:
                    logger.info('{}: has NOT enough free space ({} B) for file {}({} B). Pass'.format(info['disk_name'],
                                info['free_space'],
                                path,
                                os.path.getsize(path)))
        logger.warning('Not enough free space')
        raise raid_errors.NotEnoughFreeSpaceError

    def __gettoken(self):
        logger.info('Getting token')
        yadi = []
        for disk in config.DISKS:
            yadi.append(yadisk.YaDisk(disk['id'],
                                      disk['password']))
        for y in yadi:
            url = y.get_code_url()
            print("Go to the following url: {}".format(url))
            code = input("Enter the confirmation code: ")

            try:
                response = y.get_token(code)
            except yadisk.exceptions.BadRequestError:
                logger.critical('Bad code. Quit')
                sys.exit(-1)
            y.token = response.access_token
            if y.check_token(y.token):
                logger.info('Successfully received token!')
                self.__tokens.append(y.token)
            else:
                logger.critical('Bad token or connection problem. Quit')
                sys.exit(-1)
        self.__cachetokens()

# TODO: удалить это дерьмо, в либе яндекса уже есть свой кеш
    def __cachetokens(self):
        logger.info('Putting tokens to cache...')
        with open(config.CACHE_FILE, 'wb') as f:
            pickle.dump(self.__tokens, f)


# TODO: и это дерьмо тоже
    def __uncachetokens(self):
        logger.info('Getting tokens from cache...')
        with open(config.CACHE_FILE, 'rb') as f:
             self.__tokens = pickle.load(f)

    def returndebugshit(self):
        for token in self.__tokens:
            print(token)
            print(self._disk_info)
            print('------')
        return self.__yadi
